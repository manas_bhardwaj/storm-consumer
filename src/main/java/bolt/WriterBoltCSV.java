package bolt;
import com.cleartrip.analytics.AirDetails;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriterBoltCSV extends BaseBasicBolt {
    private static Logger LOGGER = LoggerFactory.getLogger(ConsumerBolt.class);

    /**
     * Process the input tuple and optionally emit new tuples based on the input tuple.
     * <p>
     * All acking is managed for you. Throw a FailedException if you want to fail the tuple.
     *
     * @param input
     * @param collector
     */
    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        try {
            AirDetails.airMessage airMessage = AirDetails.airMessage.parseFrom(input.getBinary(0));
            LOGGER.info(airMessage.toString());
            writeCSVLine(airMessage);
//            collector.emit(new Values(airMessage));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeCSVLine(AirDetails.airMessage airMessage) throws IOException {
        try {
            File file = new File("/home/manasbhardwaj/Personal-Workspace/Work/storm-consumer/dumps/dump.csv");
            FileWriter fr = new FileWriter(file, true);
            fr.write(airMessage.toString());
            fr.write("\n");
            fr.close();
        }
        catch (Exception e){
            LOGGER.info("writeCSVLine failed with exception  " + e);
        }
    }



    /**
     * Declare the output schema for all the streams of this topology.
     *
     * @param declarer this is used to declare output stream ids, output fields, and whether or not each output stream is a direct stream
     */
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}
