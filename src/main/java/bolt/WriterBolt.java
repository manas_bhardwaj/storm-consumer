package bolt;

import com.cleartrip.analytics.AirDetails;
import com.google.protobuf.InvalidProtocolBufferException;
import model.EventDocument;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;
import utils.WriteToElasticSearch;

import java.util.HashMap;
import java.util.Map;

public class WriterBolt extends BaseBasicBolt {

    private static final Logger LOGGER = LogManager.getLogger(WriterBolt.class);
    private static Map<String, EventDocument> eventDocumentMap = new HashMap<>();
    private int batchSize = 5;
    private WriteToElasticSearch esWriter;

    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        super.prepare(stormConf, context);
        this.esWriter = new WriteToElasticSearch();
    }

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        AirDetails.airMessage message = null;
        try {
            message = AirDetails.airMessage.parseFrom(input.getBinary(0));
//            LOGGER.info("---------------UNPROCESSED MESSAGE in writerBolt--------------\n\n" + message);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        EventDocument eventDocument = process_message(message);
//        LOGGER.info("---------------PROCESSED MESSAGE in writerBolt--------------\n\n" + eventDocument.toString());
        if(eventDocumentMap.size() >= batchSize){
            for(String key : eventDocumentMap.keySet()){
//                LOGGER.info("Event Doc is : " + eventDocumentMap.get(key) + "  User Id is :" + eventDocumentMap.get(key).getUserId());
                esWriter.addDocsToBulkProcessor(eventDocumentMap.get(key),eventDocumentMap.get(key).getUserId());
            }
            eventDocumentMap.clear();
            LOGGER.info("Batchsize reached-----Added files to addDocsToBulkProcessor function with hashmap size : " + eventDocumentMap.size());
        }
        else{
            assert eventDocument != null;
            eventDocumentMap.put(eventDocument.getUserId(), eventDocument);
//            LOGGER.info("Batchsize not reached------doc added to eventDocumentMap with map size : " + eventDocumentMap.size());
        }
    }

    private EventDocument process_message(AirDetails.airMessage message){
        EventDocument file = new EventDocument();
        try{
//            LOGGER.info("Message in process_message: " + message);
            file.setSearchQuery(message.getAirDetail().getSearchQuery());
            file.setAirLineCarrier(message.getAirDetail().getAirLineCarrier());
            file.setUserId(String.valueOf(message.getUserDetail(0).getUserId()));
            file.setFullName(message.getUserDetail(0).getFullName());
            file.setGender(message.getUserDetail(0).getGender());
            file.setAge(String.valueOf(message.getUserDetail(0).getAge()));
            file.setPassengerType(message.getUserDetail(0).getPassengerType().toString());
            file.setSource(message.getSource());
            file.setDestination(message.getDestination());
//            LOGGER.info("Processed file by process_message function is : " + new Gson().toJson(file));
            return file;
        }
        catch (Exception e){
            LOGGER.error("Returning NULL and Exception occurred in process_message function : " + e);
            return null;
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}
