import bolt.ConsumerBolt;
import bolt.WriterBolt;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MainTopology {
    private static Logger LOGGER = LoggerFactory.getLogger(MainTopology.class);
    public static void main(String[] args) throws InvalidTopologyException, AuthorizationException, AlreadyAliveException {
        String topic = "air-messages";
        String groupId = "consumer-storm-2";
        String zkRoot = "/kafkastorm";
        String topologyName = "consumer-storm-topology";
        String brokerZkPath = "/brokers";
        String spoutParallelism = "1";
        String boltParallelism = "1";
        Integer worker = 1;

        ZkHosts zkHost = new ZkHosts("localhost:2181",brokerZkPath);

        SpoutConfig spoutConfig = new SpoutConfig(zkHost,topic,zkRoot,groupId);
        spoutConfig.fetchSizeBytes = 2097152;
        spoutConfig.startOffsetTime = kafka.api.OffsetRequest.LatestTime();

        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("spout1",kafkaSpout,Integer.parseInt(spoutParallelism));
        builder.setBolt("bolt1", new ConsumerBolt(),Integer.parseInt(boltParallelism)).shuffleGrouping("spout1");
        builder.setBolt("bolt2",new WriterBolt(),Integer.parseInt(boltParallelism)).shuffleGrouping("bolt1");

        Config conf = new Config();
        conf.setDebug(false);
        conf.setNumWorkers(worker);
        conf.setMaxSpoutPending(500);
        conf.put("environment","local");
        conf.put("consumer-id",groupId);
        conf.put("topic",topic);
        conf.put("last-deployed",System.currentTimeMillis());
        conf.put("zk-root",zkRoot);
        conf.put("start_time",System.currentTimeMillis());
        conf.put("properties",loadProperties());
        try {
            StormSubmitter.submitTopologyWithProgressBar(topologyName,conf,builder.createTopology());
        }
        catch (Exception e){
            System.out.println("Storm Submitter Failed : " + e.toString());
            LOGGER.debug("Storm Submitter Failed : ",e);
        }


    }
    private static Properties loadProperties(){
        Properties prop = new Properties();
        try (InputStream input = MainTopology.class.getClassLoader()
                .getResourceAsStream("application.properties")) {
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return prop;
    }
}
