package model;

import lombok.Data;

@Data
public class EventDocument {
    /* airDetail {
        searchQuery: "Uv"
        airLineCarrier: "qSw"
    }
    userDetail {
        userId: 41657
        fullName: "mUbgHblFyd"
        gender: "N"
        age: 86
        passenger_type: ADULT
    }
     */
    private String searchQuery;
    private String airLineCarrier;
    private String userId;
    private String fullName;
    private String gender;
    private String age;
    private String passengerType;
    private String source;
    private String destination;


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getAirLineCarrier() {
        return airLineCarrier;
    }

    public void setAirLineCarrier(String airLineCarrier) {
        this.airLineCarrier = airLineCarrier;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }
}
