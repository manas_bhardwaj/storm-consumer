package utils;
import com.google.gson.Gson;
import model.EventDocument;
import org.apache.http.HttpHost;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;

public class WriteToElasticSearch {
    private Logger logger = LogManager.getLogger(WriteToElasticSearch.class);
    private String elasticCluster = "elasticsearch";
    BulkProcessor bulkProcessor;
    public RestHighLevelClient esClient;
    private int batchSize;

    BulkProcessor.Listener listener = new BulkProcessor.Listener() {
        @Override
        public void beforeBulk(long executionId, BulkRequest request) {
            logger.info("Total number of Actions : " + request.numberOfActions());

        }

        @Override
        public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
            if (response.hasFailures()) {
                logger.warn("Bulk "+ executionId +" executed with failures");
            } else {
                logger.debug("Bulk " + executionId + " completed in "
                        + response.getTook().getMillis() + "milliseconds");
            }
        }

        @Override
        public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
            logger.error("Failed to execute bulk", failure);
        }
    };

    public WriteToElasticSearch(){
        int elasticPort = 9200;
        String elasticHost = "localhost";
        this.esClient = new RestHighLevelClient(RestClient.builder(
                new HttpHost(elasticHost, elasticPort,"http")));
        this.bulkProcessor = BulkProcessor.builder(
                (request, bulkListener) -> esClient.bulkAsync(request, RequestOptions.DEFAULT, bulkListener), listener)
                .setBulkActions(batchSize)
                .setFlushInterval(TimeValue.timeValueSeconds(2))
                .build();
        batchSize = 5;

    }


    public void addDocsToBulkProcessor(EventDocument doc, String id){
        try{
            logger.info("Reached addbulk function");
            String elasticIndex = "air_messages";
            IndexRequest docRequest = new IndexRequest(elasticIndex).id(id)
                    .source(new Gson().toJson(doc),XContentType.JSON);
            bulkProcessor.add(docRequest);
//            logger.info("---------------DOCUMENT ADDED IN BULKPROCESSOR");
        }
        catch (Exception e){
            logger.error("Failed to push es " + e);
        }
    }

}
