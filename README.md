### Apache Zookeeper :

##### Start zookeeper-server
* `sudo bin/zkServer.sh start`  
    Server at : `localhost:2181`
### Apache Kafka :

##### Start kafka-server 
* `sudo bin/kafka-server-start.sh config/server.properties`

##### Create a kafka-topic
* `sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --topic air-messages --partitions 1 --replication-factor 1`

##### Get List of kafka-topics
* `bin/kafka-topics.sh --list --zookeeper localhost:2181`

##### Start a kafka-producer
* ` bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test`

##### Start a kafka-consumer
* `bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning`

### Apache Storm : 

##### Start nimbus
* `bin/storm nimbus`

##### Start supervisor
* `bin/storm supervisor`

##### Start UI
* `bin/storm ui`

##### Submit a topology
* `sudo bin/storm jar ../Personal-Workspace/Work/storm-consumer/build/libs/storm-consumer-1.0-SNAPSHOT.jar MainTopology`

Try to connect to 127.0.0.1:8090, you see Storm UI? Enjoy!

### ElasticSearch : 

##### Start ElasticSearch server

* `bin/elasticsearch`
* `bin/elasticsearch -Epath.data=data2 -Epath.logs=log2`
* `bin/elasticsearch -Epath.data=data3 -Epath.logs=log3`

##### Check cluster health

* `curl -X GET "localhost:9200/_cluster/health?pretty"`

### Kibana :

##### Start kibana server

* `bin/kibana`

Connect to http://localhost:5601/ fpor
